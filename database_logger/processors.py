from django.contrib.sessions.middleware import SessionMiddleware


class SessionDataProcessor:
    def __call__(self, logger, name, event_dict):

        request = event_dict.get('request')

        if request is not None:
            middleware = SessionMiddleware()
            middleware.process_request(request)
            session = request.session
            event_dict['session_data'] = dict(session)
        return event_dict