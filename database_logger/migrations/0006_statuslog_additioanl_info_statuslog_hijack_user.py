# Generated by Django 4.0.5 on 2023-07-05 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database_logger', '0005_alter_statuslog_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='statuslog',
            name='additioanl_info',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='statuslog',
            name='hijack_user',
            field=models.CharField(max_length=40, null=True),
        ),
    ]
