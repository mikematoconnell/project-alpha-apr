# Generated by Django 4.0.5 on 2023-07-05 19:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('database_logger', '0006_statuslog_additioanl_info_statuslog_hijack_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='statuslog',
            old_name='additioanl_info',
            new_name='additional_info',
        ),
    ]
