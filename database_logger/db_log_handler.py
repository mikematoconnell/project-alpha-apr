import logging
from django.utils import timezone
import datetime
from django.contrib.auth import get_user_model
import json


db_default_formatter = logging.Formatter()


def get_additional_information(request, status_log):
    if request.method == "GET":
        status_log.additional_info = json.dumps(request.GET.dict())
    if request.method == "POST":
        status_log.additional_info = json.dumps(request.POST.dict())
    if request.method == "PUT":
        status_log.additional_info = json.dumps(request.PUT.dict())
    if request.method == "DELETE":
        status_log.additional_info = json.dumps(request.PUT.dict())
    
    return status_log


def get_request_information(request, status_log, user_model):
    status_log.url = "%s %s" % (request.method, request.get_full_path())
    
    status_log = get_additional_information(request, status_log)
    
    hijacker_id = (request.session.get("hijack_history"))
    if hijacker_id:
        id = int(hijacker_id[0])
        hijacker = user_model.objects.get(id=id)
        status_log.hijack_user = hijacker.username
    
    return status_log


def handle_event(event, status_log):
    if event == "request_finished" or event == "request_failed":
        status_log.request_time_end = timezone.now()

        if status_log.request_time_start:
            difference = status_log.request_time_end - status_log.request_time_start
            milliseconds = int(difference.total_seconds() * 1000)
            status_log.duration = milliseconds
    
    return status_log


def get_msg_information(msg, status_log):
    status_log.msg = msg
    status_log.logger_name = msg["logger"]
    status_log.code = msg.get("code")

    return status_log


class DatabaseLogHandler(logging.Handler):
    def emit(self, record):
        from .models import StatusLog
        # trace = None

        # if record.exc_info:
        #     trace = db_default_formatter.formatException(record.exc_info)

        # if DJANGO_DB_LOGGER_ENABLE_FORMATTER:
        #     msg = self.format(record)
        # else:
        msg = record.msg
        user_model = get_user_model()

        status_log, created = StatusLog.objects.get_or_create(request_id=msg["request_id"])
        if created:
            status_log.request_time_start = timezone.now()

        status_log = get_msg_information(msg, status_log)
        
        request = msg.get("request")
        if request:
            status_log = get_request_information(request, status_log, user_model)
        
        exception = msg.get("exception")
        if exception is not None:
            status_log.trace = exception

        user_id = msg.get("user_id")
        if user_id:
            user = user_model.objects.get(pk=user_id)
            status_log.user = user
 
        event = msg.get("event")
        status_log = handle_event(event, status_log)
        
        # this prevents levels from being overwritten if log error occurs at 2nd log stage (stage 2 of 3)
        if status_log.level == logging.NOTSET or status_log.level == logging.INFO:
            status_log.level = record.levelno

        status_log.save()

    
    def format(self, record):
        if self.formatter:
            fmt = self.formatter
        else:
            fmt = db_default_formatter

        if type(fmt) == logging.Formatter:
            record.message = record.getMessage()

            if fmt.usesTime():
                record.asctime = fmt.formatTime(record, fmt.datefmt)

            # ignore exception traceback and stack info

            return fmt.formatMessage(record)
        else:
            return fmt.format(record)
