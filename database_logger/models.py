import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings

LOG_LEVELS = (
    (logging.NOTSET, _('NotSet')),
    (logging.INFO, _('Info')),
    (logging.WARNING, _('Warning')),
    (logging.DEBUG, _('Debug')),
    (logging.ERROR, _('Error')),
    (logging.FATAL, _('Fatal')),
)
USER_MODEL = settings.AUTH_USER_MODEL


# many of the fields have null=True because they are only filled after the second request goes through, or may never be filled
class StatusLog(models.Model):
    request_id = models.CharField(max_length=200, unique=True, null=True)
    code = models.CharField(max_length=100, null=True, default=4)
    logger_name = models.CharField(max_length=100)
    level = models.PositiveSmallIntegerField(choices=LOG_LEVELS, default=logging.NOTSET, db_index=True)
    msg = models.TextField()
    user = models.ForeignKey(USER_MODEL, null=True, related_name="User", on_delete=models.PROTECT)
    trace = models.TextField(blank=True, null=True)
    create_datetime = models.DateTimeField(auto_now_add=True, verbose_name='Created at')
    url = models.URLField(max_length=200, null=True)
    request_time_start = models.DateTimeField(null=True)
    request_time_end = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)
    hijack_user = models.CharField(max_length=40, null=True)
    additional_info = models.TextField(null=True)

    def __str__(self):
        return self.msg

    class Meta:
        ordering = ('-create_datetime',)
        verbose_name_plural = verbose_name = 'Logging'
