from ninja import Router
from .models import Project
from tracker.schema import UserSchema, ProjectSchema
from typing import List
from ninja.security import django_auth, HttpBearer


class AuthBearer(HttpBearer):
    def authenticate(self, request, token):
        if token == "supersecret":
            return token


router = Router()


@router.get("/bearer", auth=AuthBearer(), response=UserSchema)
def bearer(request):
    return request.user




@router.get("/", response=List[ProjectSchema], auth=django_auth)
def get_tasks(request):
    return Project.objects.all()