from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.contrib.auth import get_user_model
from hijack.views import ReleaseUserView
from django.http import HttpResponseRedirect   
from django.shortcuts import redirect
import logging
import structlog
db_logger = logging.getLogger('db')
struct_logger = structlog.get_logger("django_structlog")

# # Create your views here.


# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "list_projects"

    def get_queryset(self):

        try:
            return Project.objects.filter(members=self.request.user)
        except Exception as e:
            db_logger.exception(e)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project"

    
    def get(self, request, *args, **kwargs):
        try:
            obj = self.get_object()
            

            return super().get(request, *args, **kwargs)
        except Exception as e:

            struct_logger.exception(e)




class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]  
    template_name = "projects/create.html"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


def see(request):

    hijacker_id = int((request.session.get("hijack_history"))[0])
    user_model = get_user_model()
    hijacker = user_model.objects.get(id=hijacker_id)
    print("hh", hijacker.username)


def intermediate_hijack_release(request):
    response = ReleaseUserView.as_view()(request)

    return redirect("/admin/auth/user/")

