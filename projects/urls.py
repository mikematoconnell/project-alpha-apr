from django.urls import path
from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
    see,
    intermediate_hijack_release
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("see/", see),
    path("intermediate_release", intermediate_hijack_release, name="intermediate_release")
]
