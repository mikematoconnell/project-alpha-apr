from django.contrib import admin
from projects.models import Project, Test

# Register your models here.

admin.site.register(Test)


admin.site.register(Project)
