from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL
from auditlog_app.registry import auditlog


# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")

    def __str__(self):
        return self.name


class Test(models.Model):
    name = models.CharField(max_length=2)


auditlog.register(Project)