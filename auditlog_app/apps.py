from django.apps import AppConfig


class AuditlogConfig(AppConfig):
    name = "auditlog_app"
    verbose_name = "Audit log"
    default_auto_field = "django.db.models.AutoField"

    def ready(self):
        from auditlog_app.registry import auditlog

        auditlog.register_from_settings()
