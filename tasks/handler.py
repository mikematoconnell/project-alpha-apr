import logging
from .models import LogEntry


class DatabaseHandler(logging.Handler):
    def emit(self, record):
        log_entry = LogEntry(
            level=record.levelname,
            message=self.format(record)
        )
        log_entry.save()
