from ninja import Router
from .models import Task
from django.db.models import Q, Avg, Max, Min, Count
from tracker.schema import TaskSchema, UserSchema, NoteFoundSchema, TaskSchemaFlat
from typing import List
from ninja.security import django_auth, HttpBearer
from projects.models import Project
from django.conf import settings
from django.shortcuts import get_object_or_404

from django.contrib.auth.models import User


class AuthBearer(HttpBearer):
    def authenticate(self, request, token):
        if token == "supersecret":
            return token


router = Router()


@router.get("/bearer", auth=AuthBearer(), response=UserSchema)
def bearer(request):
    return request.user

@router.get("/", response=List[TaskSchema], auth=django_auth)
def get_tasks(request):
    return Task.objects.select_related("project").all()  


@router.get("search/", response=List[TaskSchema])
def search_tasks(request, search_param):
    return Task.objects.filter(Q(name__icontains=search_param) | Q(project__name__icontains=search_param))

@router.get("counter/")
def average(request):
    return Task.objects.aggregate(Avg("money"))


@router.post("create_task/", response={201: TaskSchema})
def create_task(request, task: TaskSchemaFlat):
    task_data = task.dict()

    project = Project.objects.get(pk=task_data["project"])
    task_data["project"] = project
    assignee = User.objects.get(pk=task_data["assignee"])
    task_data["assignee"] = assignee

    task = Task.objects.create(**task_data)
    return task

@router.get("task_detail/{task_id}", response={200: TaskSchema, 404: NoteFoundSchema})
def detail_task(request, id):
    try:
        return Task.objects.get(pk=id)
    
    except Task.DoesNotExist as e:
        return 404, {"message": "Could not find task"}

@router.put("update_task/{task_id}", response={200: TaskSchema, 404: NoteFoundSchema})
def update_task(request, task_id: int, data: TaskSchemaFlat):
    try:
        task = Task.objects.get(pk=task_id)
        
        return task
    
    except Task.DoesNotExist as e:
        return 404, {"message": "Could not find task"}
    
@router.delete("delete_task/{task_id}")
def delete_task(request, task_id:int):
    task = get_object_or_404(Task, task_id)
    task.delete()
    return {"success": True}


@router.get("task_detail_flat/{task_id}", response={200: TaskSchemaFlat, 404: NoteFoundSchema})
def detail_task_flat(request, id):
    try:
        return Task.objects.get(pk=id)
    
    except Task.DoesNotExist as e:
        return 404, {"message": "Could not find task"}