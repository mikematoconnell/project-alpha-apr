import logging
from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    # could be set as starting now, is not though
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project", related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        USER_MODEL, null=True, related_name="tasks", on_delete=models.SET_NULL
    )
    money = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class LogEntry(models.Model):
    level = models.CharField(max_length=20)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)


