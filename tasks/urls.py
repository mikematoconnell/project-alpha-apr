from django.urls import path
from tasks.views import TaskCreateView, TaskListView, update_task_is_complete, task_list, task_detail

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", update_task_is_complete, name="complete_task"),
    path("task_api/", task_list, name="task_list"),
    path("task_api/<int:id>", task_detail, name="task_detail")
]
