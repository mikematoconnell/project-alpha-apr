from rest_framework import serializers
from .models import Task
from projects.models import Project
from django.contrib.auth.models import User


class AssigneeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ["name", "description", "members"]

class TaskSerializerFlat(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ["id", "name", "start_date", "due_date", "assignee", "project"]


class TaskSerializer(serializers.ModelSerializer):
    assignee = AssigneeSerializer()
    class Meta:
        model= Task
        fields = ["id", "name", "start_date", "due_date", "assignee", "project"]

