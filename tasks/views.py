from django.views.generic.edit import CreateView
from tasks.models import Task
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .serializers import TaskSerializer
from rest_framework.response import Response
from rest_framework import status

import logging
import structlog
db_logger = logging.getLogger('db')
struct_logger = structlog.get_logger("django_structlog")

# # Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee", "money"]
    template_name = "tasks/create.html"

    def get_success_url(self):
        db_logger.info(f"{self.request.user}")
        try:
            return reverse_lazy("show_project", args=[self.object.project.id])
        except Exception as e:
            db_logger.exception(e)


class TaskListView(LoginRequiredMixin, ListView,):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "my_tasks"

    def get_queryset(self):
        


        return Task.objects.filter(assignee=self.request.user)



def update_task_is_complete(request, pk):
    try:
        task_id = pk
        Task.objects.filter(id=task_id).update(is_completed=True)
    except Exception as e:
        struct_logger.error(e)
    return redirect("show_my_tasks")


#API DJANGO REST 
@api_view(["GET", "POST"])
def task_list(request):
    if request.method == "GET":
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)

        return Response(serializer.data)

    if request.method == "POST":
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)



@api_view(["GET", "PUT", "DELETE"])
def task_detail(request, id):
    
    task = Task.objects.get(pk=id)    
    if request.method == "GET":
        serializer = TaskSerializer(task)
        return Response(serializer.data)

    elif request.method == "PUT":
        serializer = TaskSerializer(task, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP)
    
    elif request.method == "DELETE":
        task.delete()
        return Response(status=status.http_204)  