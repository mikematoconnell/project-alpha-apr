from .test_base import BaseTestCase
from .test_feature_07 import FeatureTests
from django.shortcuts import get_object_or_404


class ITestCase(BaseTestCase):
    def setUp(self):
        super.__init__()
        self.now = self.datetime.now()
        print(self.now)

    def test_if_works(self):
        now = self.datetime.now()
        print(now)


class ITTestCase(FeatureTests):

    def test_login_resolves_to_accounts_login(self):
        path = self.reverse("login")
        self.assertEqual(
            path,
            "/accounts/login/",
            msg="Could not resolve path name 'login' to '/accounts/login/",
        )
