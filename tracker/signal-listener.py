from hijack import signals


def print_hijack_started(sender, hijacker, hijacked, request, **kwargs):
    print('%d has hijacked %d' % (hijacker, hijacked))
signals.hijack_started.connect(print_hijack_started)

def print_hijack_ended(sender, hijacker, hijacked, request, **kwargs):
    print('%d has released %d' % (hijacker, hijacked))
signals.hijack_ended.connect(print_hijack_ended)