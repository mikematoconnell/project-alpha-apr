from typing import List
from datetime import datetime, date
from ninja import Schema, Field, ModelSchema
from django.conf import settings
from tasks.models import Task


USER_MODEL = settings.AUTH_USER_MODEL


class UserSchema(Schema):
    id: int = None
    username: str = None

class ProjectSchema(Schema):
    name: str = None
    description: str = None
    members: List[UserSchema] = None

# class TaskSchema(Schema):
#     name: str
#     start_date: datetime
#     due_date: datetime
#     is_completed: bool
#     project: ProjectSchema
#     assignee: UserSchema


class TaskSchema(ModelSchema):
    assignee: UserSchema
    project: ProjectSchema
    class Config:
        model = Task
        model_fields = ["name", "start_date", "due_date", "is_completed", "project", "assignee"]  

# class TaskSchemaFlat(ModelSchema):
#     class Config:
#         model = Task
#         model_fields = "__all__"

class TaskSchemaFlat(Schema):
    name: str
    start_date: datetime
    due_date: datetime
    is_completed: bool
    project_id: int
    assignee_id: int


class NoteFoundSchema(Schema):
    message: str