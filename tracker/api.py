from ninja import NinjaAPI
from tasks.api import router as tasks_router
from projects.api import router as projects_router

api = NinjaAPI(csrf=True, urls_namespace="Tracker", title="Task Tracker Application", )

api.add_router("/tasks", tasks_router, tags=["Tasks"])
api.add_router("/projects", projects_router, tags=["Projects"])
  